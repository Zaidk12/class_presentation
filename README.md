# Class in Python

Classes provide a means of bundling data and functionality together. Creating a new class creates a new type of object, allowing new instances of that type to be made.  Python classes provide all the standard features of Object Oriented Programming: the class inheritance mechanism allows multiple base classes, a derived class can override any methods of its base class or classes, and a method can call the method of a base class with the same name. 

# Object in Python

An Object is an instance of a Class. A class is like a blueprint while an instance is a copy of the class with actual values. It’s not an idea anymore, it’s an actual dog, like a dog of breed pug who’s seven years old. You can have many dogs to create many different instances, but without the class as a guide, you would be lost, not knowing what information is required.

# Namespace in Python

A namespace is a system that has a unique name for each and every object in Python. Basically, It is a function that is control all the names that we have used in our program. It will assure whatever names we have used is unique.

 ## Types of namespaces

    - Global namespace
    - Local namespaace
    - Nested namespace

### Example :

```python
# var1 is in the global namespace
var1 = 5
def some_func():

	# var2 is in the local namespace
	var2 = 6
	def some_inner_func():

		# var3 is in the nested local
		# namespace
		var3 = 7
```
# Scopes in Python

Scope refers to the coding region from which a particular Python object is accessible. Hence one cannot access any particular object from anywhere from the code, the accessing has to be allowed by the scope of the object.

## Example:

```python
# Python program showing
# a scope of object

def some_func():
	print("Inside some_func")
	def some_inner_func():
		var = 10
		print("Inside inner function, value of var:",var)
	some_inner_func()
	print("Try printing var from outer function: ",var)
some_func()
```
# A First Look at Classes:

The simplest form of class defination looks like this :

```python
class ClassName:
    <statement-1>
    .
    .
    .
    <statement-N>
```
# Class Objects:

Class objects supports two kinds opf operations : attribute references and instantiation.

## Example of attribute references :

```python
class MyClass:
    """A simple example class"""
    i = 12345

    def f(self):
        return 'hello world'
```
MyClass.i and MyClass.f are valid attribute references, returning an integer and a function object, respectively.
## Example of instantiation

Instantiating a class is creating a copy of the class which inherits all class variables and methods.

```python
class Foo():
        def __init__(self,x,y):
            print (x+y)
f = Foo(3,4)
```
# Instance Objects

A variable that is defined inside a method and belongs only to the current instance of a class.

## Example of instance objects :

```python

class Student:
    # constructor
    def __init__(self, name, age):
        # Instance variable
        self.name = name
        self.age = age

# create first object
s1 = Student("Jessa", 20)

# access instance variable
print('Object 1')
print('Name:', s1.name)
print('Age:', s1.age)

# create second object
s2= Student("Kelly", 10)

# access instance variable
print('Object 2')
print('Name:', s2.name)
print('Age:', s2.age)

```
# Method objects

Objects can also contain methods. Methods in objects are functions that belong to the object.

## Example of method objects :

```python
class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name)

p1 = Person("John", 36)
p1.myfunc()
```
__NOTE__ : The self parameter is a reference to the current instance of the class, and is used to access variables that belong to the class.

# Classes and Instance variables

**Class Variable** -  Declared inside the class definition. They are not tied to any particular object of the class, hence shared across all the objects of the class.

**Instance Variable** - Declared inside the constructor method of class. They are tied to the particular object instance of the class, hence the contents of an instance variable are completely independent from one object instance to the other.

## Example of class and instance variables :

```python
class Car:
    wheels = 4    # <- Class variable   
    def __init__(self, name):
        self.name = name    # <- Instance variable
```        
# Inheritance

Inheritance allows us to define a class that inherits all the methods and properties from another class.
Parent class is the class being inherited from, also called base class.
Child class is the class that inherits from another class, also called derived class.

## Creating a Parent Class :

```python
class Person:
  def __init__(self, fname, lname):
    self.firstname = fname
    self.lastname = lname

  def printname(self):
    print(self.firstname, self.lastname)

#Use the Person class to create an object, and then execute the printname method:

x = Person("John", "Doe")
x.printname() 
```
## Creating a Child Class :

To create a class that inherits the functionality from another class, send the parent class as a parameter when creating the child class:

```python
 class Student(Person):
  pass 
```
**Note** : Use the pass keyword when you do not want to add any other properties or methods to the class.

Now the Student class has the same properties and methods as the Person class.

```python
x = Student("Mike", "Olsen")
x.printname() 
```

# Types of Inheritance

- Single Inheritance
- Multilevel Inheritance
- Multiple Inheritance
- Hierarchical Inheritance

# Single Inheritance

Single-level inheritance enables a derived class to inherit characteristics from a single-parent class.

## Example 

```python
class Parent_class_Name:
    
      #Parent_class code block

class Child_class_Name(Parent_class_name):
     
      #Child_class code block

```
# Multilevel Inheritance

Multi-level inheritance enables a derived class to inherit properties from an immediate parent class which in turn inherits properties from his parent class.

```python
class Base1:
    pass

class Base2(Base1):
    pass

class MultiDerived(Base1, Base2):
    pass
```

# Multiple Inheritance :

When a class is derived from more than one base class it is called multiple Inheritance. The derived class inherits all the features of the base case.

## Example

```python

Class Base1:
       Body of the class

Class Base2:
     Body of the class

Class Derived(Base1, Base2):
     Body of the class

```
## Example of Multiple Inheritance:

```python
# Python Program to depict multiple inheritance
# when method is overridden in both classes

class Class1:
	def m(self):
		print("In Class1")
	
class Class2(Class1):
	def m(self):
		print("In Class2")

class Class3(Class1):
	def m(self):
		print("In Class3")
		
class Class4(Class2, Class3):
	pass
	
obj = Class4()
obj.m()
```
**Note**: When you call obj.m() (m on the instance of Class4) the output is In Class2. If Class4 is declared as Class4(Class3, Class2) then the output of obj.m() will be In Class3.

# Hierarchical Inheritance

Hierarchical level inheritance enables more than one derived class to inherit properties from a parent class.

## Example

```python
   # Here, we will create the Base class 
class Parent1:
    def func_1(self):
         print("This function is defined inside the parent class.")

# Derived class1
class Child_1(Parent1):
     def func_2(self):
        print ("This function is defined inside the child 1.")

# Derivied class2
class Child_2(Parent1):
    def func_3(self):
       print ("This function is defined inside the child 2.")

# Driver's code
object1=Child_1()
object2=Child_2()
object1.func_1()
object1.func_2()
object2.func_1()
object2.func_3()
```
# Polymorphism

polymorphism means the same function name (but different signatures) being used for different types. The key difference is the data types and number of arguments used in function.

```python

class Bird:

	def intro(self):
		print("There are many types of birds.")

	def flight(self):
		print("Most of the birds can fly but some cannot.")

class sparrow(Bird):

	def flight(self):
		print("Sparrows can fly.")

class ostrich(Bird):

	def flight(self):
		print("Ostriches cannot fly.")

obj_bird = Bird()
obj_spr = sparrow()
obj_ost = ostrich()

obj_bird.intro()
obj_bird.flight()

obj_spr.intro()
obj_spr.flight()

obj_ost.intro()
obj_ost.flight()

```
# Encapsulation

It describes the idea of wrapping data and the methods that work on data within one unit.

## Example 
```python

class Students:
   def __init__(self, name, rank, points):
      self.name = name
      self.rank = rank
      self.points = points

   # custom function
   def demofunc(self):
      print("I am "+self.name)
      print("I got Rank ",+self.rank)

# create 4 objects
st1 = Students("Steve", 1, 100)
st2 = Students("Chris", 2, 90)
st3 = Students("Mark", 3, 76)
st4 = Students("Kate", 4, 60)

# call the functions using the objects created above
st1.demofunc()
st2.demofunc()
st3.demofunc()
st4.demofunc()

```
# Data Asbtraction

It hides the unnecessary code details from the user. Also,  when we do not want to give out sensitive parts of our code implementation and this is where data abstraction came.

## Example 
```python
class MyClass:

	# Hidden member of MyClass
	__hiddenVariable = 0
	
	# A member method that changes
	# __hiddenVariable
	def add(self, increment):
		self.__hiddenVariable += increment
		print (self.__hiddenVariable)

# Driver code
myObject = MyClass()	
myObject.add(2)
myObject.add(5)

# This line causes error
print (myObject.__hiddenVariable)
```

# Private Variables 

In actual terms (practically), python doesn’t have anything called private member variable in Python. However, adding two underlines(__) at the beginning makes a variable or a method private is the convention used by most python code.

```python 

class myClass:
   __privateVar = 27;
   def __privMeth(self):
      print("I'm inside class myClass")
   def hello(self):
      print("Private Variable value: ",myClass.__privateVar)
foo = myClass()
foo.hello()
foo.__privateMeth

```
From the above output, we can see that outside the class “myClass”, you cannot access the private method as well as the private variable. However, inside the class (myClass) we can access the private variables. In the hello() method, the __privateVar variable can be accessed (as shown above: “Private Variable value: 27”).

# Iterators

An iterator is an object that can be iterated upon, meaning that you can traverse through all the values.

**Iterate using for loop**

1. To iterate list:

```python
for element in [1, 2, 3]:
    print(element)
```

2. To iterate tuple:

```python
for element in (1, 2, 3):
    print(element)
```

3.  To Iterate key or value of Dictionary:

```python
for key,value in {'one':1, 'two':2}:
    print(key)
    print(value)
```

4. To itearte character:

```python
for char in "123":
    print(char)
```

5. To iterate lines in file:
```python
for line in open("myfile.txt"):
    print(line, end='')
```
 **Iterate using iter() method**

## Example:
```python
mytuple = ("apple", "banana", "cherry")
myit = iter(mytuple)

print(next(myit))
print(next(myit))
print(next(myit))

```
# Generators
**Generator-Function**: A generator-function is defined like a normal function, but whenever it needs to generate a value, it does so with the yield keyword rather than return. If the body of a def contains yield, the function automatically becomes a generator function. 

## Example :

```python
# A generator function that yields 1 for first time,
# 2 second time and 3 third time
def simpleGeneratorFun():
	yield 1		
	yield 2		
	yield 3		

# Driver code to check above generator function
for value in simpleGeneratorFun():
	print(value)
```

